package sterlite;

import java.util.Date;

import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;

import mediationnewsnmp.SnmpV3Configurations;
import sterlite.SNMPAlertConstants.SEVERITY;

public class SNMP4JAlertSender {
	private static final String MODULE = "[SNMPAlertSender]";

	static final boolean isAndvanceTrap = true;
	String host = "127.0.0.1";
	int port = 42083;
	String comunity = "public";
	int version = SnmpConstants.version1;
	SEVERITY alertSeverity = SNMPAlertConstants.SEVERITY.INFO;
	String alertMessage = "Test msg";
	//V3
	private static final int SECURITY_LEVEL = SecurityLevel.AUTH_PRIV;
	private static final String USERNAME = "kamleshSA";
	//private static final OID AUTH_PROTOCOL = AuthMD5.ID;
	private static final OID AUTH_PROTOCOL = AuthSHA.ID;
	private static final String AUTH_PASSWORD = "12345678";
	private static final OID PRIV_PROTOCOL = Priv3DES.ID;
	private static final String PRIV_PASSWORD = "12345678";
	private Date startTime = new Date();

	private static final String DEFAULT_PROTOCOL = "udp:";
	
	SnmpV3Configurations snmpV3Conf = new SnmpV3Configurations(SECURITY_LEVEL, USERNAME, AUTH_PROTOCOL, AUTH_PASSWORD,
			PRIV_PROTOCOL, PRIV_PASSWORD);
	Snmp snmp = null;
	
	public static void main(String arg[]) {
		new SNMP4JAlertSender().sendTrap("From Demo Project");
		new SNMP4JAlertSender().close();
	}

	public SNMP4JAlertSender() {
		final String subModule = MODULE + "[SNMPAlertSender] ";
		try {
			TransportMapping<?> transport = new DefaultUdpTransportMapping();
			snmp = new Snmp(transport);
			transport.listen();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(subModule + " Exception while creating SNMP.Ex:" + ex);
		}
	}

	public void sendTrap(String msg) {
		final String subModule = MODULE + "[sendTrap] ";
		System.out.println(subModule + " SNMPAlert received " + msg);
		if(version != SnmpConstants.version3)
			sendV1orV2Trap("1.3.6.1.4.1.21067.13.1.1.5.3", alertSeverity.getValue(), alertMessage);
		else
			sendSnmpV3Trap("1.3.6.1.4.1.21067.13.1.1.5.3", alertSeverity.getValue(), alertMessage);
	}

	private PDU createPdu(int snmpVersion, SNMPAlertConstants.SEVERITY requestType, String mib) {
		PDU pdu = null;
		if(snmpVersion == SnmpConstants.version1)
			pdu = new PDUv1();
		else if(snmpVersion == SnmpConstants.version3)
			pdu = new ScopedPDU();
		else
			pdu = DefaultPDUFactory.createPDU(snmpVersion);
		if (snmpVersion == SnmpConstants.version1) {
			pdu.setType(PDU.V1TRAP);
			PDUv1 pdu1 = (PDUv1) pdu;
			pdu1.setGenericTrap(PDUv1.ENTERPRISE_SPECIFIC);
			pdu1.setSpecificTrap(SNMPAlertConstants.SNMP_SPECIFIC);
			pdu1.setEnterprise(new OID(SNMPAlertConstants.SNMP_APPLICATION_MIB));
		} else {
			if (requestType == SNMPAlertConstants.SEVERITY.TRAP) {
				pdu.setType(PDU.TRAP);
			} else if (requestType == SNMPAlertConstants.SEVERITY.INFO) {
				pdu.setType(PDU.INFORM);
			}
		}
		long sysUpTime = System.currentTimeMillis() - startTime.getTime();
		TimeTicks tt = new TimeTicks();
		tt.fromMilliseconds(sysUpTime);
		pdu.add(new VariableBinding(SnmpConstants.sysUpTime, tt));
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID(mib))); //should be before setting other binding
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapAddress, new IpAddress(host)));
		pdu.add(new VariableBinding(new OID(mib), new OctetString(alertMessage)));
		if (isAndvanceTrap) {
			pdu.add(new VariableBinding(SNMPAlertConstants.AlertVariables4.ALERT_SEVERITY.getOid(),
					new OctetString(String.valueOf(requestType.getValue()))));
			pdu.add(new VariableBinding(SNMPAlertConstants.AlertVariables4.SERVER_INSTANCE.getOid(),
					new OctetString("INSTANCE 1")));
		}
		return pdu;
	}

	private void sendV1orV2Trap(String mib, int alertSeverity, String alertMessage) {
		try {
			// create v1/v2 PDU
			PDU snmpPDU = createPdu(version, SNMPAlertConstants.SEVERITY.fromInt(alertSeverity), mib);
			// Create Target
			CommunityTarget comtarget = new CommunityTarget();
			comtarget.setCommunity(new OctetString(comunity));
			comtarget.setVersion(version);
			comtarget.setAddress(new UdpAddress(host + "/" + port));
			comtarget.setRetries(2);
			comtarget.setTimeout(5000);
			snmp.send(snmpPDU, comtarget);
			System.out.println("Sent Trap to (IP:Port)=> " + host + ":" + port);
		} catch (Exception e) {
			System.err.println("Error in Sending Trap to (IP:Port)=> " + host + ":" + port);
			System.err.println("Exception Message = " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Sends the v3 trap
	 */
	private void sendSnmpV3Trap(String mib, int alertSeverity, String alertMessage) {
		try {
			Address targetAddress = GenericAddress.parse(DEFAULT_PROTOCOL + host + "/" + port);
			USM usm = new USM(SecurityProtocols.getInstance().addDefaultProtocols(), new OctetString(MPv3.createLocalEngineID()),
					0);
			SecurityProtocols.getInstance().addPrivacyProtocol(new PrivDES());
			SecurityModels.getInstance().addSecurityModel(usm);
			snmp.getUSM().addUser(new OctetString(snmpV3Conf.getSnmpV3User()),
					new UsmUser(new OctetString(snmpV3Conf.getSnmpV3User()), snmpV3Conf.getAuthAlgorithmOid(),
							new OctetString(snmpV3Conf.getAuthPassword()), snmpV3Conf.getPrivAlgorithmOid(),
							new OctetString(snmpV3Conf.getPrivPassword())));

			// Create Target
			UserTarget target = new UserTarget();
			target.setAddress(targetAddress);
			target.setRetries(1);
			target.setTimeout(11500);
			target.setVersion(SnmpConstants.version3);
			target.setSecurityLevel(snmpV3Conf.getSecurityLevel());
			target.setSecurityName(new OctetString(snmpV3Conf.getSnmpV3User()));
			PDU pdu = createPdu(version, SNMPAlertConstants.SEVERITY.fromInt(alertSeverity), mib);
			snmp.send(pdu, target);
			System.out.println("Sending Trap to (IP:Port)=> " + host + ":" + port);
			snmp.addCommandResponder(new CommandResponder() {
				public void processPdu(CommandResponderEvent arg0) {
					System.out.println(arg0);
				}
			});
			System.out.println("Sent Trap to (IP:Port)=> " + host + ":" + port);
		} catch (Exception e) {
			System.err.println("Error in Sending Trap to (IP:Port)=> " + host + ":" + port);
			System.err.println("Exception Message = " + e.getMessage());
		}
	}

	public void close() {
		try {
			snmp.close();
		} catch (Exception ex) {
		}
	}
}
