package sterlite;


import org.snmp4j.smi.OID;

import com.sun.management.snmp.SnmpOid;

public class SNMPAlertConstants {

	public static String SNMP_APPLICATION_MIB = "1.3.6.1.4.1.21067.13";
	public static String SNMP_TEST_MIB = SNMP_APPLICATION_MIB+".1.1.4.5";
	public static int SNMP_ENTERPRISE_SPECIFIC = 6;
	public static int SNMP_SPECIFIC = 0;

	public enum Version {
		SNMPV1(1),SNMPV2(2);
		private int value;

		Version(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static Version fromInt(int i) {
			for (Version b : values()) {
				if (b.getValue() == i) {
					return b;
				}
			}
			return null;
		}
	}
	public enum SEVERITY {
		INFO(1), TRAP(2);
		private int value;

		SEVERITY(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static SEVERITY fromInt(int i) {
			for (SEVERITY b : values()) {
				if (b.getValue() == i) {
					return b;
				}
			}
			return null;
		}
	}

	public enum Alerts {
		SERVER_UP(1), SERVER_DOWN(2), DATA_SOURCE_UP(3), DATA_SOURCE_DOWN(4), LICENSE_EXPIRED(5);
		private int value;

		Alerts(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public enum AlertVariables {
		ALERT_SEVERITY(new SnmpOid(SNMP_APPLICATION_MIB + "1.1.2")), SERVER_INSTANCE(
				new SnmpOid(SNMP_APPLICATION_MIB + ".1.1.5.4"));
		private SnmpOid oid;

		AlertVariables(SnmpOid oid) {
			this.oid = oid;
		}

		public SnmpOid getOid() {
			return oid;
		}
	}
	
	public enum AlertVariables4 {
		ALERT_SEVERITY(new OID(SNMP_APPLICATION_MIB + "1.1.2")), SERVER_INSTANCE(
				new OID(SNMP_APPLICATION_MIB + ".1.1.5.4"));
		private OID oid;

		AlertVariables4(OID oid) {
			this.oid = oid;
		}

		public OID getOid() {
			return oid;
		}
	}

}
