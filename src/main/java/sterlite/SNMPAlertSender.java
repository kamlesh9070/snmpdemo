package sterlite;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import com.sun.management.comm.SnmpV3AdaptorServer;
import com.sun.management.snmp.SnmpDefinitions;
import com.sun.management.snmp.SnmpOid;
import com.sun.management.snmp.SnmpStatusException;
import com.sun.management.snmp.SnmpString;
import com.sun.management.snmp.SnmpVarBind;
import com.sun.management.snmp.SnmpVarBindList;
import com.sun.management.snmp.manager.SnmpParameters;
import com.sun.management.snmp.manager.SnmpPeer;
import com.sun.management.snmp.manager.SnmpSession;

public class SNMPAlertSender {

	private static final String MODULE = "[SNMPAlertSender]";

	boolean isAndvanceTrap = true;
	String host = "127.0.0.1";
	int port = 42083;
	String comunity = "public";
	int version = 2;
	String user = "kamlesh";
	String password = "123456";
	String context = "context1";
	private SnmpV3AdaptorServer snmpAdaptor = null;
	private SnmpSession snmpSession = null;

	public SNMPAlertSender() {
		final String subModule = MODULE + "[SNMPAlertSender] ";

		//snmpAdaptor = new SnmpV3AdaptorServer();
		snmpAdaptor = new SnmpV3AdaptorServer(false,42083,InetAddress.getLoopbackAddress());
		try {
			snmpSession = new SnmpSession("EliteSNMPSession");
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(subModule + " Exception generation SNMP Session.Ex:" + ex);
		}
	}

	public void sendTrap(String msg) {

		final String subModule = MODULE + "[sendTrap] ";
		System.out.println(subModule + " SNMPAlert received " + msg);
		sendSNMPALert("1.3.6.1.4.1.21067.13.1.1.5.3", 1, msg);
	}


	public void sendSNMPALert(String mib, int alertSeverity, String alertMessage) {
		final String subModule = MODULE + "[sendSNMPALert] ";

		List<SnmpVarBind> extraParams = new ArrayList<>();
		if (isAndvanceTrap) {
			extraParams.add(new SnmpVarBind(SNMPAlertConstants.AlertVariables.ALERT_SEVERITY.getOid(),
					new SnmpString(String.valueOf(alertSeverity))));
			extraParams
					.add(new SnmpVarBind(SNMPAlertConstants.AlertVariables.SERVER_INSTANCE.getOid(), new SnmpString("INSTANCE 1")));
		}
		try {
			System.out.println(subModule + " SNMPAlert sending to KK" + " at " + host + ":" + port + " #Message:" + alertMessage);
			SnmpPeer snmpPeer = new SnmpPeer(host, port);
			SnmpParameters parameters = new SnmpParameters();
			parameters.setRdCommunity(comunity);
			parameters.setWrCommunity(comunity);
			parameters.setInformCommunity(comunity);
			snmpPeer.setParams(parameters);
			generateSNMPAlert(SNMPAlertConstants.Version.fromInt(version), SNMPAlertConstants.SEVERITY.fromInt(alertSeverity),
					SNMPAlertConstants.SNMP_ENTERPRISE_SPECIFIC, SNMPAlertConstants.SNMP_SPECIFIC, snmpPeer,
					SNMPAlertConstants.SNMP_APPLICATION_MIB, new SnmpOid(mib), alertMessage, extraParams);
		} catch(SnmpStatusException se) {
			se.printStackTrace();
			System.out.println(se.getErrorIndex() + "\t" + se.getMessage());
			
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(subModule + " Error while sending SNMP Alert to snmp listener . Reason: " + ex);
		}
	}

	private void generateSNMPAlert(SNMPAlertConstants.Version snmpversion, SNMPAlertConstants.SEVERITY requestType, int generic,
			int specific, SnmpPeer snmpPeer, String serverOid, SnmpOid oid, String message, List<SnmpVarBind> extraParameters)
			throws IOException, SnmpStatusException {
		final String subModule = MODULE + "[generateSNMPAlert] ";

		SnmpVarBind varBind = new SnmpVarBind(oid, new SnmpString(message));
		SnmpVarBindList varBindList = new SnmpVarBindList(2);
		varBindList.addVarBind(varBind);
		if (extraParameters != null && !extraParameters.isEmpty()) {
			for (SnmpVarBind snmpVarBind : extraParameters) {
				varBindList.addVarBind(snmpVarBind);
			}
		}
		int securityLevel = SnmpDefinitions.authNoPriv;
		try {
			if (snmpversion == SNMPAlertConstants.Version.SNMPV1) {
				snmpAdaptor.snmpV1Trap(snmpPeer, null, new SnmpOid(serverOid), generic, specific, varBindList, null);
			} else if (snmpversion == SNMPAlertConstants.Version.SNMPV2) {
				if (requestType == SNMPAlertConstants.SEVERITY.TRAP) {
					snmpAdaptor.snmpV2Trap(snmpPeer, oid, varBindList, null);
				} else if (requestType == SNMPAlertConstants.SEVERITY.INFO) {
					snmpSession.snmpInformRequest(snmpPeer, null, oid, varBindList);
				}
			} else {
				((SnmpV3AdaptorServer) snmpAdaptor).snmpV3UsmTrap("defaultUser", securityLevel, "TEST-CONTEXT",
						new SnmpOid("1.2.3.4.5.6.7.8.9.0"), varBindList);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(subModule + " Error while sending SNMP Alert to snmp listener . Reason: " + e);
		} catch (SnmpStatusException e) {
			e.printStackTrace();
			System.out.println(subModule + " Error while sending SNMP Alert to snmp listener . Reason: " + e);
		}
	}

	public void close() {
		try {
			snmpAdaptor.stop();
		} catch (Exception ex) {
		}
		try {
			snmpSession.destroySession();
		} catch (Exception ex) {
		}

	}
}
