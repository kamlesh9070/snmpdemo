package sterlite;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;

import com.sun.jdmk.comm.HtmlAdaptorServer;
import com.sun.management.comm.SnmpAdaptorServer;
import com.sun.management.comm.SnmpV3AdaptorServer;

public class Agent3 {

	static SnmpV3AdaptorServer snmpAdaptor = null;

	private static int nbTraps = -1;

	public static void main(String args[]) {
        
        final MBeanServer server;
        final ObjectName htmlObjName;
        final ObjectName snmpObjName;
        final ObjectName mibObjName;
        final ObjectName trapGeneratorObjName;
        int htmlPort = 8082;
        int snmpPort = 161;

        try {
            server = MBeanServerFactory.createMBeanServer();
            String domain = server.getDefaultDomain();

            // Create and start the HTML adaptor.
            //
            htmlObjName = new ObjectName(domain + 
                 ":class=HtmlAdaptorServer,protocol=html,port=" 
                 + htmlPort);
            HtmlAdaptorServer htmlAdaptor = new HtmlAdaptorServer(htmlPort);
            server.registerMBean(htmlAdaptor, htmlObjName);
            htmlAdaptor.start();
                  
            // Create and start the SNMP adaptor.
            //
            snmpObjName = new ObjectName(domain + 
                 ":class=SnmpAdaptorServer,protocol=snmp,port=" + snmpPort);
            snmpAdaptor = new SnmpV3AdaptorServer(snmpPort);
            server.registerMBean(snmpAdaptor, snmpObjName);
	           snmpAdaptor.registerUsmMib(server, null);
            snmpAdaptor.start();

            // Send a coldStart SNMP Trap. 
            // Use port = snmpPort+1.
            //
            snmpAdaptor.setTrapPort(new Integer(snmpPort+1));
            snmpAdaptor.snmpV1Trap(0, 0, null);
			System.out.println("Done.");
      
            // Create the MIB II (RFC 1213) and add it to the MBean server.
            //
            mibObjName= new ObjectName("snmp:class=RFC1213_MIB");
			/*
			 * RFC1213_MIB mib2 = new RFC1213_MIB_IMPL(); server.registerMBean(mib2,
			 * mibObjName);
			 */
      
            // Bind the SNMP adaptor to the MIB 
			// snmpAdaptor.addMib(mib2, "TEST-CONTEXT");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public static SnmpAdaptorServer getSnmpAdaptor() {
		return snmpAdaptor;
	}
}