package snmp4jexample;


import java.util.Date;

import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;

/**
 * @author mchopker
 * 
 */
public class SNMPTrapGeneratorClient2 {

	private static final String community = "public";
	private static final String trapOid = ".1.3.6.1.6.3.1.1.5.1";
	private static final String ipAddress = "127.0.0.1";
	private static final int port = 42083;
	private static final String USERNAME = "kamleshAP";
	private static final String AUTH_PASSWORD = "12345678";
	private static final String PRIV_PASSWORD = "12345678";
	private static final String msg = "MajorKK";
	private static final int SECURITY_LEVEL = SecurityLevel.AUTH_PRIV;
	private static final boolean isAdvanceTrap = true;
	private static final Date systemStartTime = new Date();
	public static void main(String args[]) {
		//sendSnmpV1V2Trap(SnmpConstants.version1);
		//sendSnmpV1V2Trap(SnmpConstants.version2c);
		sendSnmpV3Trap();
		//sendV3();
	}

	/**
	 * This methods sends the V1/V2 trap
	 * 
	 * @param version
	 */
	private static void sendSnmpV1V2Trap(int version) {
		// send trap
		sendV1orV2Trap(version, community, ipAddress, port);
	}

	private static PDU createPdu(int snmpVersion) {
		PDU pdu = DefaultPDUFactory.createPDU(snmpVersion);
		if (snmpVersion == SnmpConstants.version1) {
			pdu.setType(PDU.V1TRAP);
		} else {
			pdu.setType(PDU.TRAP);
		}
		pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new OctetString(String.valueOf(new Date().getTime()))));
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID(trapOid)));
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapAddress,
				new IpAddress(ipAddress)));
		pdu.add(new VariableBinding(new OID(trapOid), new OctetString("Major")));
		return pdu;
	}

	private static void sendV1orV2Trap(int snmpVersion, String community,
			String ipAddress, int port) {
		try {
			// create v1/v2 PDU
			PDU snmpPDU = createPdu(snmpVersion);

			// Create Transport Mapping
			TransportMapping<?> transport = new DefaultUdpTransportMapping();
			transport.listen();

			// Create Target
			CommunityTarget comtarget = new CommunityTarget();
			comtarget.setCommunity(new OctetString(community));
			comtarget.setVersion(snmpVersion);
			comtarget.setAddress(new UdpAddress(ipAddress + "/" + port));
			comtarget.setRetries(2);
			comtarget.setTimeout(5000);

			// Send the PDU
			Snmp snmp = new Snmp(transport);
			snmp.send(snmpPDU, comtarget);
			System.out.println("Sent Trap to (IP:Port)=> " + ipAddress + ":"
					+ port);
			snmp.close();
		} catch (Exception e) {
			System.err.println("Error in Sending Trap to (IP:Port)=> "
					+ ipAddress + ":" + port);
			System.err.println("Exception Message = " + e.getMessage());
		}
	}

	/**
	 * Sends the v3 trap
	 */
	private static void sendSnmpV3Trap() {
		try {
			Address targetAddress = GenericAddress.parse("udp:" + ipAddress
					+ "/" + port);
			TransportMapping<?> transport = new DefaultUdpTransportMapping();
			Snmp snmp = new Snmp(transport);
			USM usm = new USM(SecurityProtocols.getInstance()
					.addDefaultProtocols(), new OctetString(
					MPv3.createLocalEngineID()), 0);
			SecurityProtocols.getInstance().addPrivacyProtocol(new PrivDES());
			SecurityModels.getInstance().addSecurityModel(usm);
			transport.listen();

			snmp.getUSM().addUser(
					new OctetString(USERNAME),
					new UsmUser(new OctetString(USERNAME), AuthMD5.ID,
							new OctetString(AUTH_PASSWORD), PrivDES.ID,
							new OctetString(PRIV_PASSWORD)));
			
			// Create Target
			UserTarget target = new UserTarget();
			target.setAddress(targetAddress);
			target.setRetries(1);
			target.setTimeout(11500);
			target.setVersion(SnmpConstants.version3);
			target.setSecurityLevel(SECURITY_LEVEL);
			target.setSecurityName(new OctetString(USERNAME));

			// Create PDU for V3
			ScopedPDU pdu = new ScopedPDU();
			pdu.setType(PDU.TRAP);
			pdu.add(new VariableBinding(SnmpConstants.sysUpTime,new OctetString(new Date().getTime() + "")));
			//pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID,SnmpConstants.linkDown));
			pdu.add(new VariableBinding(new OID(trapOid), new OctetString(msg)));

			// Send the PDU
			snmp.send(pdu, target);
			System.out.println("Sending Trap to (IP:Port)=> " + ipAddress + ":"
					+ port);
			snmp.addCommandResponder(new CommandResponder() {
				public void processPdu(CommandResponderEvent arg0) {
					System.out.println(arg0);
				}
			});
			snmp.close();
		} catch (Exception e) {
			System.err.println("Error in Sending Trap to (IP:Port)=> "
					+ ipAddress + ":" + port);
			System.err.println("Exception Message = " + e.getMessage());
		}
	}
	
	
	/*private static void sendV3() {
		try {
	        // set udpAdress and transportMapping
	        Address targetAddress = GenericAddress.parse("udp:" + ipAddress + "/" + port);
	        TransportMapping transport = new DefaultUdpTransportMapping();
	        Snmp snmp = new Snmp(transport);

	        // Protocole de sécurité +usm +snmp
	        SecurityProtocols.getInstance().addDefaultProtocols();
	        final USM usm = new USM(SecurityProtocols.getInstance(),
	            new OctetString(MPv3.createLocalEngineID(new OctetString())),
	            0);

	        SecurityProtocols.getInstance().addPrivacyProtocol(new PrivDES());
	        SecurityModels.getInstance().addSecurityModel(usm);

	        transport.listen();

	        // Ajout d'un user avec les paramètres de sécurité
	        snmp.getUSM().addUser(new OctetString("kamleshA"),
	            new UsmUser(new OctetString("kamleshA"), AuthMD5.ID,
	                new OctetString("12345678"), PrivDES.ID,
	                new OctetString("12345678")));

	        // Create Target
	        UserTarget target = new UserTarget();
	        target.setAddress(targetAddress);
	        target.setRetries(1111);
	        target.setTimeout(11111500);
	        target.setVersion(SnmpConstants.version3);
	        target.setSecurityLevel(SecurityLevel.AUTH_NOPRIV);
	        target.setSecurityName(new OctetString("kamleshA"));

	        // Create PDU 1 for V3
	        ScopedPDU pdu = new ScopedPDU();
	        pdu.setType(PDU.TRAP);
	        pdu.add(new VariableBinding(SnmpConstants.sysUpTime));
	        pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, SnmpConstants.linkDown));
	        pdu.add(new VariableBinding(new OID(trapOid), new OctetString("V33333")));
	        snmp.send(pdu, target);

	        System.out.println("Sending Trap to (IP:Port)=> " + ipAddress + ":" + port);

	        snmp.addCommandResponder(new CommandResponder() {
	            public void processPdu(CommandResponderEvent arg0) {
	                System.out.println(arg0);
	            }
	        });
	        snmp.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}*/
}
