package iReasoning;

import com.sun.management.snmp.SnmpVarBindList;

/**
 * This example demonstrates how to send SNMPv1, v2c, and v3 traps
 * 
 * <pre>
 * Example:
 * java snmptrap -s 10333 -n .1.3.6.1.2.1.2.2.1.1 -g 2 localhost
 * java snmptrap -s 10333 -n .1.3.6.1.2.1.2.2.1.1 -g 6 -i 20 localhost
 * java snmptrap -t 2 -s 10333 -q .1.3.6.1.2.1.2.2.1.1 localhost
 * java snmptrap -t 2 -s 10333 -q .1.3.6.1.2.1.2.2.1.7 localhost 1.3.6.1.2.1.2.2.1.7.3 i 2 //with variable ifAdminStatus.3
 * java snmptrap -v 3 -u newUser -A abc12345 -X abc12345 -e 12345  -s 10333 -q .1.3.6.1.2.1.2.2.1.1 localhost
 * 
 * </pre>
 */
public class TrapSender {

	SnmpVarBindList _vblist = new SnmpVarBindList();

	public static void main(String[] args) {
		
		//snmptrap.main();
	}

	protected  static void printExample(String programName)
    {
        System.out.println( "java " + programName + 
                " -s 10333 -n .1.3.6.1.2.1.2.2.1.1 -g 2 localhost" ); 
        System.out.println( "java " + programName + 
                " -s 10333 -n .1.3.6.1.2.1.2.2.1.1 -g 6 -i 20 localhost" ); 
        System.out.println( "java " + programName + 
                " -t 2 -s 10333 -q .1.3.6.1.2.1.2.2.1.1 " + "localhost" );
        System.out.println( "java " + programName + 
                " -t 2 -s 10333 -q .1.3.6.1.2.1.2.2.1.7 " + "localhost 1.3.6.1.2.1.2.2.1.7.3 i 1" );
        System.out.println( "java " + programName + 
                " -v 3 -u newUser -A abc12345 -X abc12345 -s 10333 -q .1.3.6.1.2.1.2.2.1.1 " +
                "localhost" );
        System.out.println( "java " + programName + 
                " -v 3 -u newUser -A abc12345 -X abc12345 -e 12345  -s 10333 -q .1.3.6.1.2.1.2.2.1.1 " +
                "localhost" );
    }
}// end of class
