package mediationnewsnmp;


public class SystemAlert {
	
	private String alertId;
	private String name;
	private String serverity;
	private String generatorIdentity;
	private String message;
	private String oid;

	public SystemAlert(String alertId, String name, String serverity,
			String generatorIdentity, String oid , String message) {
		this.alertId = alertId;
		this.name = name;
		this.serverity = serverity;
		this.generatorIdentity = generatorIdentity;
		this.oid = oid;
		this.message = message;
	}

	public String getAlertId() {
		return alertId;
	}

	public String getName() {
		return name;
	}

	public String getServerity() {
		return serverity;
	}

	public String getGeneratorIdentity() {
		return generatorIdentity;
	}

	public String getOid() {
		return oid;
	}

	public String getMessage() {
		return message;
	}
}

