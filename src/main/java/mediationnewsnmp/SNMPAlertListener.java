package mediationnewsnmp;


import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import org.snmp4j.Snmp;
import org.snmp4j.UserTarget;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModel;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OctetString;

/*import org.snmp4j.Snmp;
import org.snmp4j.UserTarget;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModel;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OctetString;*/

import com.sun.management.comm.SnmpV3AdaptorServer;
import com.sun.management.snmp.SnmpDefinitions;
import com.sun.management.snmp.SnmpEngineId;
import com.sun.management.snmp.manager.SnmpParameters;
import com.sun.management.snmp.manager.SnmpPeer;
import com.sun.management.snmp.manager.usm.SnmpUsmParameters;
import com.sun.management.snmp.manager.usm.SnmpUsmPeer;


/**
 * @author elitecore
 * SNMP Alert Listener can be configuration of local or remote machine which will receive alerts.
 */
public class SNMPAlertListener {

	private static final String MODULE = "SNMPALERTLISTENER";
	/**
	 * IP Address of SNMP Listener
	 */
	private String ipAddress;
	/**
	 * Port of SNMP Listener
	 */
	private String port;

	/**
	 * @see public class com.sun.management.snmp.manager.SnmpPeer
	 */
	private SnmpPeer snmpPeer;

	/**
	 * Version information for SNMP Alert
	 * @see com.elitecore.core.commons.alert
	 */
	private TrapVersion version;
	/**
	 * whether SNMP Listener is advanced or not
	 */
	private boolean isAdvanced;

	/**
	 * Specification community of SNMP Listener
	 */
	private String community;

	/**
	 * List of Alerts allocated for this SNMP Listener
	 */
	private List<String> alertIdList;
	
	/**
	 * SnmpV3 Configuration for this SNMP Listener
	 */
	private SnmpV3Configurations snmpV3Conf;
	
	/**
	 * SnmpV3 Adaptor for this SNMP Listener
	 */
	private Snmp snmpV3Adaptor;
	
	/**
	 * Snmp V3 DES Adaptor for this SNMP Listener
	 */
	private SnmpV3AdaptorServer snmpV3AdaptorServer;
	
	/**
	 * SnmpV3 UserTarget for this SNMP Listener
	 */
	private UserTarget snmpV3UserTarget;
	
	
	/**
	 * @see public class com.sun.management.snmp.manager.usm.SnmpUsmPeer
	 */
	private SnmpUsmPeer snmpUsmPeer;
	
	private static final String DEFAULT_PROTOCOL = "udp:";
	
	/**
	 * @param id Setting id value for SNMP Listener
	 * @param name Setting name value for SNMP Listener
	 * @param ipAddress Setting IP address value for SNMP Listener
	 * @param port Setting port value for SNMP Listener
	 * @param community Setting community value for SNMP Listener
	 * @param trapVersion Setting trapVersion value for SNMP Listener
	 * @param bAdvance Setting bAdvance value for SNMP Listener
	 * @param list Setting List of alert value for SNMP Listener
	 * @param snmpV3Conf setting for Snmp V3 support 
	 */
	public SNMPAlertListener(String id, String name, String ipAddress, String port, String community,
			TrapVersion trapVersion, boolean bAdvance, List<String> list, SnmpV3Configurations snmpV3Conf, Snmp snmpV3Adaptor,
			SnmpV3AdaptorServer snmpV3AdaptorServer) {
		version = trapVersion;
		isAdvanced = bAdvance;
		this.ipAddress = ipAddress;
		this.port = port;
		this.community = community;
		alertIdList = list;
		this.snmpV3Conf = snmpV3Conf;
		this.snmpV3Adaptor = snmpV3Adaptor;
		this.snmpV3AdaptorServer = snmpV3AdaptorServer;
	}

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.IAlertListener#init()
	 */
	public void init() {

		SnmpParameters parameters =new SnmpParameters();
		parameters.setRdCommunity(community);
		int localPort = 42083;
		try {
			localPort = Integer.parseInt(this.port);
		}catch (Exception e) {
			e.printStackTrace();
		}

		try {
			// SnmpPeer tries to resolve host if it fails it will throw UnknownHostException
			snmpPeer = new SnmpPeer(ipAddress,localPort);
			snmpPeer.setParams(parameters);
			
			//Snmp V3 Configuration
			if(version == TrapVersion.V3){
				
				if((snmpV3Conf.getSecurityLevel() == SecurityLevel.NOAUTH_NOPRIV || snmpV3Conf.getSecurityLevel() == SecurityLevel.AUTH_NOPRIV) ||
						(snmpV3Conf.getPrivAlgorithmName() != null && snmpV3Conf.getPrivAlgorithmName().equalsIgnoreCase(SnmpV3Configurations.DES_PRIV_ALGO_STRING))){
					InetAddress addr = InetAddress.getByName(ipAddress);
					snmpV3AdaptorServer.registerUsmMib();
					SnmpEngineId engineId = SnmpEngineId.createEngineId(addr, Integer.valueOf(port));
					snmpUsmPeer = new SnmpUsmPeer(snmpV3AdaptorServer.getEngine(), addr,  engineId);
					SnmpUsmParameters paramsV3 = new SnmpUsmParameters( snmpV3AdaptorServer.getEngine(), snmpV3Conf.getSnmpV3User());
					paramsV3.setPrincipal(snmpV3Conf.getSnmpV3User());
					paramsV3.setContextEngineId(engineId.toString().getBytes());
					if(snmpV3Conf.getSecurityLevel() == SecurityLevel.NOAUTH_NOPRIV){
						paramsV3.setSecurityLevel(SnmpDefinitions.noAuthNoPriv);
					}else if(snmpV3Conf.getSecurityLevel() == SecurityLevel.AUTH_PRIV){
						paramsV3.setSecurityLevel(SnmpDefinitions.authPriv);
					}else{
						paramsV3.setSecurityLevel(SnmpDefinitions.authNoPriv);	
					}
					paramsV3.setProtocolVersion(3);
					snmpUsmPeer.setTimeout(30000);
					snmpUsmPeer.setDestPort(Integer.valueOf(port));
					snmpUsmPeer.setMaxTries(3);
					snmpUsmPeer.setParams(paramsV3);
					snmpV3UserTarget = null;
				}else{
					if(snmpV3Conf.getSnmpV3User() == null || 
							snmpV3Conf.getSnmpV3User().trim().length() == 0 ){
						System.out.println("Incorrect SNMP V3 User configuration");
					}
					snmpV3Adaptor.getUSM().addUser(new OctetString(snmpV3Conf.getSnmpV3User().trim()),
							new UsmUser(new OctetString(snmpV3Conf.getSnmpV3User()),
							snmpV3Conf.getAuthAlgorithmOid(), new OctetString(snmpV3Conf.getAuthPassword()), 
							snmpV3Conf.getPrivAlgorithmOid(), new OctetString(snmpV3Conf.getPrivPassword())));
					snmpV3UserTarget = new UserTarget();
					snmpV3UserTarget.setSecurityName(new OctetString(snmpV3Conf.getSnmpV3User()));
					snmpV3UserTarget.setAddress(GenericAddress.parse(DEFAULT_PROTOCOL + ipAddress+ "/" + port));
					snmpV3UserTarget.setVersion(3);
					snmpV3UserTarget.setSecurityLevel(snmpV3Conf.getSecurityLevel());
					snmpV3UserTarget.setSecurityModel(SecurityModel.SECURITY_MODEL_USM);
					snmpUsmPeer = null;
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ISNMPAlertListener#getVersion()
	 */
	public TrapVersion getVersion() {
		return version;
	}

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ISNMPAlertListener#isAdvancedTrap()
	 */
	public boolean isAdvancedTrap() {
		return isAdvanced;
	}

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ISNMPAlertListener#getSnmpPeer()
	 */
	public SnmpPeer getSnmpPeer() {
		return snmpPeer;
	}
	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.IAlertListener#getAlertIds()
	 */
	public List<String> getAlertIds(){
		return alertIdList;
	}
	
	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.IAlertListener#getAlertIds()
	 */
	public UserTarget getSnmpV3UserTarget(){
		return snmpV3UserTarget;
	}
	
	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.IAlertListener#getAlertIds()
	 */
	public SnmpUsmPeer getSnmpUsmPeer(){
		return snmpUsmPeer;
	}
}
