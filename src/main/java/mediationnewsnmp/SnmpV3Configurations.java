package mediationnewsnmp;

import org.snmp4j.smi.OID;

public class SnmpV3Configurations {

	private OID authAlgorithmOid;
	private OID privAlgorithmOid;
	private int securityLevel = 0;
	private String hostIpAddress;
	private int hostPort;
	private String snmpV3User;
	private String authPassword;
	private String privPassword;
	private boolean isConfigureSecurityFile = false;
	
	public SnmpV3Configurations() {
		
	}
	
	public SnmpV3Configurations(int securityLevel, String snmpV3User, OID authAlgorithmOid, String authPassword,
			OID privAlgorithmOid, String privPassword) {
		super();
		this.authAlgorithmOid = authAlgorithmOid;
		this.privAlgorithmOid = privAlgorithmOid;
		this.securityLevel = securityLevel;
		this.snmpV3User = snmpV3User;
		this.authPassword = authPassword;
		this.privPassword = privPassword;
	}
	public static final String DES_PRIV_ALGO_STRING = "usmDESPrivProtocol";
	public static final String MD5_AUTH_ALGO_STRING = "usmHMACMD5AuthProtocol";
	public static final String SHA_AUTH_ALGO_STRING = "usmHMACSHAAuthProtocol";
	
	/** The Constant Security File Name . */
	public static final String SECURITY_FILE_NAME = "jdmk.security";
	
	/** The Constant UACL File Name . */
	public static final String UACL_FILE_NAME = "jdmk.uacl";
	
	/** The Constant ALERT Folder Name . */
	public static final String ALERT_FOLDER_NAME = "alert";
	
	/** The Constant DICTIONARY Folder Name . */
	public static final String DICTIONARY_FOLDER_NAME = "dictionary";
	
	private String authAlgorithmName;
	private String privAlgorithmName;
	
	public String getSnmpV3User() {
		return snmpV3User;
	}
	public void setSnmpV3User(String snmpV3User) {
		this.snmpV3User = snmpV3User;
	}
	public OID getAuthAlgorithmOid() {
		return authAlgorithmOid;
	}
	public void setAuthAlgorithmOid(OID authAlgorithmOid) {
		this.authAlgorithmOid = authAlgorithmOid;
	}
	public OID getPrivAlgorithmOid() {
		return privAlgorithmOid;
	}
	public void setPrivAlgorithmOid(OID privAlgorithmOid) {
		this.privAlgorithmOid = privAlgorithmOid;
	}
	public int getSecurityLevel() {
		return securityLevel;
	}
	public void setSecurityLevel(int securityLevel) {
		this.securityLevel = securityLevel;
	}
	public String getHostIpAddress() {
		return hostIpAddress;
	}
	public void setHostIpAddress(String hostIpAddress) {
		this.hostIpAddress = hostIpAddress;
	}
	public int getHostPort() {
		return hostPort;
	}
	public void setHostPort(int hostPort) {
		this.hostPort = hostPort;
	}
	public String getAuthPassword() {
		return authPassword;
	}
	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}
	public String getPrivPassword() {
		return privPassword;
	}
	public void setPrivPassword(String privPassword) {
		this.privPassword = privPassword;
	}
	public boolean isConfigureSecurityFile() {
		return isConfigureSecurityFile;
	}
	public void setConfigureSecurityFile(boolean isConfigureSecurityFile) {
		this.isConfigureSecurityFile = isConfigureSecurityFile;
	}
	public String getAuthAlgorithmName() {
		return authAlgorithmName;
	}
	public void setAuthAlgorithmName(String authAlgorithmName) {
		this.authAlgorithmName = authAlgorithmName;
	}
	public String getPrivAlgorithmName() {
		return privAlgorithmName;
	}
	public void setPrivAlgorithmName(String privAlgorithmName) {
		this.privAlgorithmName = privAlgorithmName;
	}
	
}
