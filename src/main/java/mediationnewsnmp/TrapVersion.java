package mediationnewsnmp;

import java.util.HashMap;
import java.util.Map;

public enum TrapVersion {
	
	V1("v1", 0),
	V2("v2c", 1),
	V3("v3", 2);
	
	private static Map<Integer,TrapVersion> map;
	
	static{
		map = new HashMap<Integer, TrapVersion>(2);
		for(TrapVersion trapVersion :  values()){
			map.put(trapVersion.value, trapVersion);
		}
	}
	
	private String name;
	
	private int value;
	
	private TrapVersion(String name, int value) {
		this.name = name;
		this.value = value;
	}
		 
	public static TrapVersion fromValue(int value){
		return map.get(value);
	}
		
	public String getName() {
		return name;
	}
	public int getValue() {
		return value;
	}
}
