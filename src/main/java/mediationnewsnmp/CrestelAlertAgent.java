package mediationnewsnmp;



import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.MPv3;
import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.PrivAES192;
import org.snmp4j.security.PrivAES256;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.sun.management.comm.SnmpV3AdaptorServer;
import com.sun.management.snmp.SnmpEngineParameters;
import com.sun.management.snmp.SnmpOid;
import com.sun.management.snmp.SnmpOidRecord;
import com.sun.management.snmp.SnmpOidTable;
import com.sun.management.snmp.SnmpStatusException;
import com.sun.management.snmp.SnmpString;
import com.sun.management.snmp.SnmpTimeticks;
import com.sun.management.snmp.SnmpVarBind;
import com.sun.management.snmp.SnmpVarBindList;
import com.sun.management.snmp.IPAcl.JdmkAcl;
import com.sun.management.snmp.agent.SnmpMib;

public class CrestelAlertAgent {

	private static final String MODULE = "CRESTEL ALERT AGENT";
	private static final int GENERICENTERPRISESPECIFIC = 6;
	private static final int SPECIFIC = 0;
	private static final String ALERTSEVERITY = "alertSeverity";
	
	/** The Constant Security File Name . */
	public static final String SECURITY_FILE_NAME = "jdmk.security";
	
	/** The Constant UACL File Name . */
	public static final String UACL_FILE_NAME = "jdmk.uacl";
	
	/** The Constant ALERT Folder Name . */
	public static final String ALERT_FOLDER_NAME = "alert";
	
	/** The Constant DICTIONARY Folder Name . */
	public static final String DICTIONARY_FOLDER_NAME = "dictionary";
	
	private SnmpOid enterpriseOid;

	private SnmpV3AdaptorServer snmpAdaptor;
	private ArrayList<SnmpOidTable> snmpTrapTables;
	private Snmp snmpV3Adaptor;
	private TransportMapping<?> transport = null;

	private String ipAddress;
	private int port;
	private Date startupTime;
	private int engineBoots = 0;

	public CrestelAlertAgent(SnmpOid enterpriseOid, String ipAddress, int port) {
		this.enterpriseOid = enterpriseOid;
		snmpTrapTables = new ArrayList<>();
		this.ipAddress = ipAddress;
		this.port = port;
	}
	
	public static void main(String arg[]) {
		String ipAddress = "127.0.0.1";
		int port = 42083;
		String user = "kamleshA";
		String authPassword = "12345678";
		SystemAlert systemAlert = new SystemAlert("1", "Test", "INFO", "1", ".1.3.6.1.6.3.1.1.4.1.0", "### Test from Code");
		List<String> list = new ArrayList();
		list.add("Server Up");
		SnmpV3Configurations snmpV3Conf = new SnmpV3Configurations();
		snmpV3Conf.setSecurityLevel(SecurityLevel.AUTH_NOPRIV);
		snmpV3Conf.setSnmpV3User(user);
		snmpV3Conf.setAuthPassword(authPassword);
		snmpV3Conf.setAuthAlgorithmName(SnmpV3Configurations.MD5_AUTH_ALGO_STRING);
		snmpV3Conf.setPrivAlgorithmName(SnmpV3Configurations.DES_PRIV_ALGO_STRING);
		snmpV3Conf.setPrivPassword(authPassword);
		String enterpriseOid = ".1.3.6.1.6.3.1.1.5.1";
		CrestelAlertAgent crestelAlertAgent = new CrestelAlertAgent(new SnmpOid(enterpriseOid), ipAddress, port);
		crestelAlertAgent.init();
		SNMPAlertListener snmpAlertListener = new SNMPAlertListener("1", "Listener", "127.0.0.1", "42083", "public",
				TrapVersion.V3, true, list, snmpV3Conf, crestelAlertAgent.getSnmpV3Adaptor(),
				crestelAlertAgent.getSnmpV3AdaptorServer());
		snmpAlertListener.init();
		crestelAlertAgent.sendTrap(systemAlert, snmpAlertListener);
	}


	public CrestelAlertAgent(SnmpOid enterpriseOid, String ipAddress, int port,Date startupTime) {
		this.enterpriseOid = enterpriseOid;
		snmpTrapTables = new ArrayList<>();
		this.ipAddress = ipAddress;
		this.port = port;
		this.startupTime=startupTime;
	}

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ICrestelAlertAgent#init()
	 */
	public void init() {
		System.out.println(MODULE + "Initializing Crestel Snmp Agent on ip - " + ipAddress + " and port - " + port);
		startupTime = new Date();
		InetAddress snmpInetAddress = null;
		
		String securityFile = SECURITY_FILE_NAME;
		File jdmkSecurityFile = new File(securityFile);
		try {
			snmpInetAddress = InetAddress.getByName(ipAddress);
		} catch (UnknownHostException e) {
			System.out.println(MODULE + e);
			System.out.println("Invalid IPAddress : "+ipAddress+" configured in Snmp-conf.xml");
		}
		
		try {
			JdmkAcl jdmkAcl = new JdmkAcl("CrestelJdmkAcl","jdmk.acl");
			
			 if(jdmkSecurityFile.exists()){
				 
				 SnmpEngineParameters engineParameters = new SnmpEngineParameters();
				 engineParameters.activateEncryption();
				 engineParameters.setSecurityFile(securityFile);
				 snmpAdaptor = new SnmpV3AdaptorServer(engineParameters, null, false,port, snmpInetAddress);
			 }else{
				 snmpAdaptor = new SnmpV3AdaptorServer(jdmkAcl,port, snmpInetAddress);
			 }
			 System.out.println(MODULE +"Engine Id for DES SNMP engine **************** "+snmpAdaptor.getEngineId().toString());
			 
			snmpAdaptor.enableSnmpV1V2SetRequest();
/*			RFC1213_MIB rfc1213MIB = new RFC1213_MIB();
			rfc1213MIB.init();
			snmpAdaptor.addMib(rfc1213MIB);*/
			snmpAdaptor.start();
			
			try{
				//Snmp V3 Configuration. V3 Engine port will be default incremented by 1 
				UdpAddress udpAddress = new UdpAddress(snmpInetAddress, port+1);
				transport = new DefaultUdpTransportMapping(udpAddress);
				 
				snmpV3Adaptor = new Snmp(transport);
				engineBoots = engineBoots + 1;
				OctetString aesEngineId = new OctetString(MPv3.createLocalEngineID());
				USM usm = new USM(SecurityProtocols.getInstance(), aesEngineId,engineBoots);
				System.out.println(MODULE + "Engine Id for AES SNMP engine **************** "+new OctetString(MPv3.createLocalEngineID()).toHexString().replace(":", ""));
				SecurityModels.getInstance().addSecurityModel(usm);
				SecurityProtocols.getInstance().addPrivacyProtocol(new PrivAES192());
				SecurityProtocols.getInstance().addPrivacyProtocol(new PrivAES256());
				SecurityProtocols.getInstance().addPrivacyProtocol(new PrivAES128());
				SecurityProtocols.getInstance().addPrivacyProtocol(new Priv3DES());
				SecurityProtocols.getInstance().addPrivacyProtocol(new PrivDES());
				SecurityProtocols.getInstance().addAuthenticationProtocol(new AuthMD5());
				SecurityProtocols.getInstance().addAuthenticationProtocol(new AuthSHA());
				//transport.listen();
				System.out.println(MODULE + "Crestel Snmp Agent Initialized successfully on ip - " + ipAddress + " and port - " + port);
			}catch(Exception ex){
				ex.printStackTrace();
				System.out.println(MODULE + "Crestel Snmp Agent Initialization unsuccessfully on ip - " + ipAddress + " and port - " + port);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ICrestelAlertAgent#sendTrap(com.elitecore.core.commons.alert.SystemAlert, com.elitecore.core.commons.alert.ISNMPAlertListener)
	 */
	public void sendTrap(SystemAlert alert,SNMPAlertListener snmpListener) {
		
		SnmpOid oid;
		SnmpOidRecord oidRecord = resolveName(alert.getName());
		ScopedPDU pdu = null;
		if(oidRecord == null){
			oid = new SnmpOid(alert.getOid());
		}else{
			oid = new SnmpOid(oidRecord.getOid());
		}
		SnmpOidRecord severityRecord = resolveName(ALERTSEVERITY);
		TrapVersion trapVersion = snmpListener.getVersion();

		SnmpVarBind varBind = new SnmpVarBind(oid, new SnmpString(alert.getMessage()));
		SnmpVarBindList varBindList = new SnmpVarBindList(2);
		varBindList.addVarBind(varBind);

		pdu = new ScopedPDU();
		pdu.setType(ScopedPDU.TRAP);
		VariableBinding v3Variable = new VariableBinding(new OID(oid.toString()), new OctetString(alert.getMessage()));
		pdu.add(v3Variable);
		
		if(snmpListener.isAdvancedTrap()) {
			if(severityRecord == null){
				System.out.println(MODULE + "Sending Advanced trap without alertSeverity. Reason: alert Oid for AlertSeverity: "
						+ ALERTSEVERITY +" not resolved");
			}else{
				SnmpOid serverityOid = new SnmpOid(severityRecord.getOid());
				SnmpVarBind serverityVarBind = new SnmpVarBind(serverityOid, new SnmpString(alert.getServerity()));
				varBindList.addVarBind(serverityVarBind);
				
				OID serverityOidAes = new OID(severityRecord.getOid());
				VariableBinding v3SeverVariable = new VariableBinding(serverityOidAes, new OctetString(alert.getServerity()));
				pdu.add(v3SeverVariable);
			}
		}

		System.out.println(MODULE + "Sending SNMP Alert: " + alert.getName() + " to snmp peer: " + snmpListener.getSnmpPeer().getDestAddr());

		try {
			
			if( trapVersion == TrapVersion.V1){
				// agentAddr will be local address
				// If the local host cannot be determined, it takes 0.0.0.0
				// SnmpTimeticks will be ( Current Time -  Adaptor Startup time )
				snmpAdaptor.snmpV1Trap(snmpListener.getSnmpPeer(), null, enterpriseOid, GENERICENTERPRISESPECIFIC, SPECIFIC , varBindList, null);
			}else if( trapVersion == TrapVersion.V2){
				snmpAdaptor.snmpV2Trap(snmpListener.getSnmpPeer(), oid, varBindList, null);
			}else if(trapVersion == TrapVersion.V3){
				if(snmpListener.getSnmpUsmPeer() != null){
					
					SnmpTimeticks sysUpTimeValue = new SnmpTimeticks(getSysUpTime()) ;
					snmpAdaptor.snmpV3UsmTrap(snmpListener.getSnmpUsmPeer(), oid, varBindList, sysUpTimeValue);
				}else{
					snmpV3Adaptor.send(pdu,snmpListener.getSnmpV3UserTarget());
				}
			}
			System.out.println("Successfully sent Alert");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(MODULE + "Error occured while sending  SNMP Alert to snmp peer: " + snmpListener.getSnmpPeer().getDestAddr() +
					". Reason: " + e.getMessage());
		}
	}

	private SnmpOidRecord resolveName(String alertName){
		SnmpOidRecord oidRecord = null;
		for(SnmpOidTable oidTable : snmpTrapTables){
			try{
				oidRecord = oidTable.resolveVarName(alertName);
				break;
			} catch (SnmpStatusException e) {
				e.printStackTrace();
			}
		}
		return oidRecord;
	}
	
	public long getSysUpTime() {
        return (System.currentTimeMillis() - startupTime.getTime()) / 10 ;
    }

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ICrestelAlertAgent#registerMib(com.sun.management.snmp.agent.SnmpMib)
	 */
	public void registerMib(SnmpMib snmpMib){

		if(snmpMib == null){
			System.out.println(MODULE + "Can't register Snmp Mib to Crestel Alert Agent. Reason: "
					+ "Snmp Mib is null");
		}else{
			snmpMib.setSnmpAdaptor(snmpAdaptor);
		}
	}

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ICrestelAlertAgent#registetSnmpTrapTable(com.sun.management.snmp.SnmpOidTable)
	 */
	public void registetSnmpTrapTable(SnmpOidTable snmpOidTable){

		if(snmpOidTable == null){
			System.out.println(MODULE + "Can't register Snmp Oid Table to Crestel Alert Agent. Reason: "
					+ "Snmp Oid Table is null");
			return;
		}
		snmpTrapTables.add(snmpOidTable);
	}

	/* (non-Javadoc)
	 * @see com.elitecore.core.commons.alert.ICrestelAlertAgent#stop()
	 */
	public void stop(){
		System.out.println(MODULE + "Stopping Crestel Alert Agent");
		snmpAdaptor.stop();
		try {
			snmpV3Adaptor.close();
			transport.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(MODULE + "Crestel Alert Agent Stopped Successfully");
	}


	public int getProcessEntityBatchSize() {
		return 0;
	}

	public SnmpOid getEnterpriseOid() {
		return enterpriseOid;
	}


	public void setEnterpriseOid(SnmpOid enterpriseOid) {
		this.enterpriseOid = enterpriseOid;
	}


	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public Date getStartupTime() {
		return startupTime;
	}


	public void setStartupTime(Date startupTime) {
		this.startupTime = startupTime;
	}
	
	public Snmp getSnmpV3Adaptor() {
        return snmpV3Adaptor;
    }
	
	public SnmpV3AdaptorServer getSnmpV3AdaptorServer() {
        return snmpAdaptor;
    }
	
}
